﻿using BenchmarkDotNet.Running;
using System;
using System.Collections.Generic;
using System.IO;

namespace Words
{
    class Program
    {
        static Words words = new Words();
        static void Main(string[] args)
        {
            Console.WriteLine("Введите любой текст:");
            var text = Console.ReadLine();
            var dict = words.CreateWordFrequencyDict(text);

            PrintResult(dict);
            Console.ReadLine();
        }

        static void PrintResult(Dictionary<string, int> dict)
        {
            Console.WriteLine(new string('-', 20));
            foreach (var item in dict)
            {
                Console.WriteLine(item.Key.PadRight(20) + " " + item.Value);
            }
        }
    }
}
