﻿using BenchmarkDotNet.Attributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace Words
{
    public class Words
    {
        char[] _separators = new char[] { ' ', ',', '.', ':', ';', '!', '?' };
        string[] _splittedWords;
        Dictionary<string, int> WordsFrequency { get; }


        public Words()
        {
            WordsFrequency = new Dictionary<string, int>();
        }

        public Dictionary<string, int> CreateWordFrequencyDict(string text)
        {
            Parse(text);
            return WordsFrequency;
        }
        public Dictionary<string, int> CreateWordFrequencyDict(FileInfo fileInfo)
        {
            using (StreamReader reader = new StreamReader(fileInfo.FullName))
            {
                while (!reader.EndOfStream)
                {
                    Parse(reader.ReadLine());
                }
            }
            return WordsFrequency;
        }

        public void Parse(string text)
        {
            _splittedWords = text.Split(_separators);
            foreach (var item in _splittedWords)
            {
                if (WordsFrequency.ContainsKey(item)) continue;
                WordsFrequency.Add(item, GetQuantity(item, _splittedWords));
            }
        }

        private int GetQuantity(string word, string[] wordsArray)
        {
            var quantity = 0;
            foreach (var item in wordsArray)
            {
                if (item == word) quantity++;
            }
            return quantity;
        }
    }
}
