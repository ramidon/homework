﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Collections;

namespace DynamicArray
{
    public class DynamicArray<T> : IEnumerable<T>, IEnumerator<T>
    {
        private T[] _items;
        public int Length { get; private set; } = 0;

        private bool _arrayIsFilled => Capacity == Length;
        public int Capacity
        {
            get { return _items.Length; }
            set
            {
                if (_items == null)
                {
                    _items = new T[value];
                }
                else if (_items.Length != value)
                {
                    var _newItems = new T[value];
                    Array.Copy(_items, 0, _newItems, 0, Length);
                    _items = _newItems;
                }
            }
        }

        #region Constructors
        public DynamicArray()
        {
            _items = new T[8];
        }
        public DynamicArray(int capacity)
        {
            Capacity = capacity;
        }
        public DynamicArray(IEnumerable<T> items) : this(items.Count())
        {
            foreach (var item in items)
            {
                Add(item);
            }
        }
        #endregion

        public void Add(T item)
        {
            if (item == null) throw new NullReferenceException();

            if (_arrayIsFilled)
            {
                Capacity *= 2;
            }
            _items[Length++] = item;
        }
        public void AddRange(IEnumerable<T> items)
        {
            if (items == null) throw new NullReferenceException();
            if ((Capacity - Length) < items.Count())
            {
                Capacity += items.Count();
            }
            foreach (var item in items)
            {
                Add(item);
            }
        }
        public bool Remove(T item)
        {
            if (item == null) throw new NullReferenceException();
            if (!_items.Contains(item)) throw new ArgumentOutOfRangeException();
            var index = (_items as IList<T>).IndexOf(item);
            Array.Copy(_items, index + 1, _items, index, Length - index - 1);
            Length--;
            return true;
        }
        public bool Insert(T item, int index)
        {
            if (index >= Length) throw new ArgumentOutOfRangeException();
            if (item == null) throw new NullReferenceException();
            if (_arrayIsFilled) Capacity *= 2;
            Array.Copy(_items, index, _items, index + 1, Length - index);
            _items[index] = item;
            Length++;
            return true;
        }

        #region IEnumerable,IEnumerator impl
        public T Current => _items[position];
        object IEnumerator.Current => Current;

        int position = -1;
        public bool MoveNext()
        {
            if (position >= Length - 1)
            {
                Reset();
                return false;
            }
            position++;
            return true;
        }

        public void Reset()
        {
            position = -1;
        }

        public void Dispose()
        {

        }

        public IEnumerator<T> GetEnumerator()
        {
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion
    }
}
