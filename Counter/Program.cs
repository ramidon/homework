﻿using System;
using System.Linq;

namespace Counter
{
    class Program
    {
        static void Main(string[] args)
        {
            int count = 0;
            Console.WriteLine("Считалочка\nВведите количество участников");

            while (!int.TryParse(Console.ReadLine(), out count))
            {
                PrintError("Введено неправильное значение. Введите целочисленное значение");
            }

            var list = Enumerable.Range(1, count).ToList();
            Counter<int> counter = new Counter<int>();

            counter.Count(list);
            Console.WriteLine();

            Console.ReadLine();
        }

        static void PrintError(string message)
        {
            Console.WriteLine(message);
        }
    }
}
