﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Counter
{
    public class Counter<T>
    {
        public void Count(List<T> rawItems)
        {
            PrintItems(rawItems);
            for (int i = 0; i < rawItems.Count; i++)
            {
                rawItems.RemoveAt(i);
            }

            if (rawItems.Count < 1)
            {
                return;
            }
            Count(rawItems);
        }

        private void PrintItems(List<T> items)
        {
            foreach (var item in items)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
        }
    }
}
